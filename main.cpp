#include "simgrid/s4u.hpp"
#include <iostream>
#include <stdlib.h>
#include <cmath>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include "simgrid/plugins/live_migration.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(example, "Messages specific for this example");


double calculateDataTime( double data_size, int links_size)
{
  double theLatency = 0.00225 * links_size;
  double alpha = 13.01;
  double bandwidthRatio = 0.97;
  double windowSize = 4194304   *  8 ;//# bits
  data_size = data_size *  8 ;//# bits
  double bandwidth = 1e9;
  double throughputWithLatency =  (windowSize / (2. * theLatency));//  # only in version 3.28 it seems to be multiplied by the bandwidth factor
  double throughputWithBandwidth = (bandwidth)/1.05;
  double throughput = std::min(throughputWithLatency, throughputWithBandwidth) * bandwidthRatio;
  double estimatedTime =   3*alpha * theLatency + data_size / throughput;
  return estimatedTime;
}



static void vm_migrate(simgrid::s4u::VirtualMachine * vm,simgrid::s4u::Host *source, simgrid::s4u::Host *dest, std::ofstream *output)
{  
    double mig_sta = simgrid::s4u::Engine::get_clock();    
    sg_vm_migrate(vm, dest); 
    double mig_end =simgrid::s4u::Engine::get_clock(); 
    *output << (mig_end - mig_sta)  <<";"<< source->get_name() << ";" <<dest->get_name() << ";" << vm->get_core_count() <<  ";" <<  simgrid::s4u::Engine::get_clock()<< ";" << std::endl;    
    simgrid::s4u::this_actor::exit();
}


static void master()
{
  std::string migs_file = "results/migrations_data_to_process";
  std::ifstream input; 
	std::ofstream output;
  output.open("results/migrations_data_no_congestion.csv");   // the final name will be 'migrations_data_to_processed.csv'
  input =  std::ifstream(migs_file+".csv");
  std::string  nextLine;     
  int total_migs = 0;

  while(std::getline(input,nextLine))
  {        
    total_migs++;
    std::vector<std::string> columns;				        
    boost::algorithm::split(columns, nextLine, boost::is_any_of(";"));
    std::string source_host_name = columns[2].c_str();    
    std::string target_host_name = columns[3].c_str();
    std::string vm_name = columns[1].c_str();
    int vm_cores = atoi(columns[0].c_str());        
    double time_it_took_to_migrate = atof(columns[4].c_str());    
    double estimated_time =atof(columns[5].c_str());        
    simgrid::s4u::Host * source_host = simgrid::s4u::Host::by_name(source_host_name);
    simgrid::s4u::Host * target_host = simgrid::s4u::Host::by_name(target_host_name);        
    simgrid::s4u::VirtualMachine *vm = new simgrid::s4u::VirtualMachine(vm_name,source_host,vm_cores);      
    long long ram = vm_cores * 2L * 1024 * 1024 * 1024;    
    vm->set_ramsize(ram);
    vm->start();
    output << vm_name << ";" << time_it_took_to_migrate << ";" << estimated_time << ";";     
    simgrid::s4u::Actor::create("mig"+vm_name,target_host,vm_migrate,vm,source_host,target_host,&output);    
    simgrid::s4u::this_actor::sleep_for(1000.0);
    vm->destroy();    
    //std::cout << "ram "<< ram << ";" <<std::endl;
    //std::cout << "Cores "<< vm_cores << ";" <<std::endl;
    std::vector<simgrid::s4u::Link *> links;                            
    target_host->route_to(source_host,links,nullptr);
    
  //  std::cout << "estimado emmm "<< calculateDataTime(ram,links.size()) << ";" <<std::endl;
   // break; 

  }

  std::cout << "total migs"<< total_migs << ";" <<std::endl;
}

int main(int argc, char* argv[])
{
  simgrid::s4u::Engine e(&argc, argv);
  sg_vm_live_migration_plugin_init();
  e.load_platform(argv[1]);
  simgrid::s4u::Actor::create("master", simgrid::s4u::Host::by_name("adonis-1.grenoble.grid5000.fr"),master);
  e.run();
  return 0;
}